# arcos-pykdl

Python bindings for ARCOS-KDL

ARCOS-KDL is not available in PyPi yet, so you will have in install in from the
repository, or use test.pypi.org as yhe repository.

For installing from source:
Once you have created a Python3 virtual environment, and you are inside it, execute:
```.bash
pip install maturin cffi
cd arcos-pykdl
maturin develop --release
```

To build a wheel:
```.bash
pip install maturin cffi
cd arcos-pykdl
maturin build --release
```
the wheel will be placed on target/wheels

As a quick example, inside a IPython shell run:

```.python
import arcos_pykdl as kdl

f = kdl.Frame([[1,0,0,4],[0,1,0,8],[0,0,1,12],[0,0,0,1]])
t = kdl.Twist([1,2,3,4,5,6])

kdl.add_delta(f, t, 0.1)
```

You should get:

```
[[0.7140753634021542, -0.43216494552774987, 0.5507538790050222, 4.1],
 [0.6196565105099439, 0.7562609655231478, -0.20998847827591904, 8.2],
 [-0.3257640010263893, 0.49122582574921003, 0.8078211458932512, 12.3],
 [0, 0, 0, 1]]
```
