# ARCOS-KDL

## v0.2.0
- Remove joints, segments, chains, and solvers from the API. Users are intended
  to use the kinematic Arm instead.
- Add syntax sugar for adding, subtracting and multiplying geometric types
- Add syntax sugar for indexing geometric types
- Make geometric types iterables

## v0.1.1
- Add documentation
- Prevent panics in favor of raising exceptions

## v0.1.0
- First release
