// Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
// Author: Daniel Garcia-Vaglio <degv364@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use pyo3::prelude::*;
use pyo3::wrap_pyfunction;

use arcos_kdl::geometry::add_delta as kdl_add_delta;
use arcos_kdl::geometry::diff as kdl_diff;

use crate::frame::{Frame, TupleFrame};
use crate::twist::{Twist, TupleTwist};

/// Add a Twist to a Frame. (kinematic integration)
/// Return a tuple that will be used to
/// Create a Frame in Python
#[pyfunction]
pub fn add_delta(pose: &Frame, twist: &Twist, dt: f64) -> PyResult<TupleFrame> {
    let rt = kdl_add_delta(pose.frame, twist.twist, dt);
    Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)], rt[(0,3)]),
        (rt[(1,0)], rt[(1,1)], rt[(1,2)], rt[(1,3)]),
        (rt[(2,0)], rt[(2,1)], rt[(2,2)], rt[(2,3)]),
        (0.0, 0.0, 0.0, 1.0)))
}

/// Calculate the Twist between two frames. (kinematic differentiation)
/// Return a tuple that will be used to
/// Create a Frame in Python
#[pyfunction]
pub fn diff(end: &Frame, init: &Frame, dt: f64) -> PyResult<TupleTwist> {
    let rt = kdl_diff(end.frame, init.frame, dt);
    Ok((rt[0], rt[1], rt[2], rt[3], rt[4], rt[5]))
}

/// Module for geometric functions
#[pymodule]
pub fn _geometry(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_wrapped(wrap_pyfunction!(diff))?;
    m.add_wrapped(wrap_pyfunction!(add_delta))?;
    Ok(())
}
