// Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
// Author: Daniel Garcia-Vaglio <degv364@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use pyo3::prelude::*;
use pyo3::exceptions;
use pyo3::PyObjectProtocol;
use pyo3::PyIterProtocol;

use arcos_kdl::geometry::{EulerBuild, Introspection, invert_frame};
use arcos_kdl::geometry::Frame as KdlFrame;

use crate::vector::TupleVector;
use crate::rotation::TupleRotation;

/// 4D vector
pub type FrameRow = (f64, f64, f64, f64);
/// Tuple with the same size as a 4X4 Matrix
pub type TupleFrame = (FrameRow, FrameRow, FrameRow, FrameRow);

/// Determine if the size of a matrix fits the description
fn is_matrix_size_correct(matrix: Vec<Vec<f64>>) -> bool{
    if matrix.len() != 4 {
        false
    } else {
        // The last row is always 001, so I don't care
        matrix[0].len() == 4 && matrix[1].len() == 4 && matrix[2].len() == 4
    }
}


/// Homogeneous Matrix
#[pyclass(subclass, name=_Frame)]
#[derive(Clone, Copy)]
pub struct Frame {
    /// ARCOS-KDL Frame
    pub frame: KdlFrame,
    /// Used for iterating
    pub idx: usize,
}

#[pymethods]
impl Frame {
    /// Create a new Homogeneous matrix from a normal 4x4 matrix
    #[new]
    pub fn new(in_array: Vec<Vec<f64>>) -> PyResult<Self> {
        if !is_matrix_size_correct(in_array.clone()) {
            Err(exceptions::ValueError::py_err("Wrong matrix size"))
        } else {
            let mut tmp_frame = KdlFrame::zeros();
            for i in 0..4 {
                for j in 0..4 {
                    tmp_frame[(i, j)] = in_array[i][j];
                }
            }
            Ok(Self { frame: tmp_frame , idx: 0})
        }
    }

    /// Get the 'formal' string of this Frame
    pub fn _repr(&self) -> PyResult<String> {
        Ok(format!(
            "[[{}, {}, {}, {}],\n [{}, {}, {}, {}],\n [{}, {}, {}, {}],\n [{}, {}, {}, {}]]",
            self.frame[(0,0)].to_string(),
            self.frame[(0,1)].to_string(),
            self.frame[(0,2)].to_string(),
            self.frame[(0,3)].to_string(),
            self.frame[(1,0)].to_string(),
            self.frame[(1,1)].to_string(),
            self.frame[(1,2)].to_string(),
            self.frame[(1,3)].to_string(),
            self.frame[(2,0)].to_string(),
            self.frame[(2,1)].to_string(),
            self.frame[(2,2)].to_string(),
            self.frame[(2,3)].to_string(),
            self.frame[(3,0)].to_string(),
            self.frame[(3,1)].to_string(),
            self.frame[(3,2)].to_string(),
            self.frame[(3,3)].to_string()))

    }

    fn _next(&mut self) -> Option<FrameRow> {
        if self.idx < 4 {
            self.idx += 1;
            Some((self.frame[(self.idx-1, 0)],
                  self.frame[(self.idx-1, 1)],
                  self.frame[(self.idx-1, 2)],
                  self.frame[(self.idx-1,3)]))
        } else {
            self.idx = 0;
            None
        }
    }

    /// Get the vector representing the translation of the Frame
    /// Return a tuple that will be used to
    /// Create a Frame in Python
    pub fn _get_translation(&self) -> PyResult<TupleVector> {
        let rt = self.frame.get_translation();
        Ok((rt[0], rt[1], rt[2]))
    }

    /// Get the Rotation representing the rotation of the Frame
    /// Return a tuple that will be used to
    /// Create a Frame in Python
    pub fn _get_rotation(&self) -> PyResult<TupleRotation> {
        let rt = self.frame.get_rotation();
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)])))
    }

    /// Get the Euler angles representing the rotation of the Frame
    pub fn get_euler(&self) -> PyResult<(f64, f64, f64)> {
        Ok(self.frame.get_euler())
    }

    /// Compute the inverse of a Frame
    /// Return a tuple that will be used to
    /// Create a Frame in Python
    pub fn _inverse(&self) -> PyResult<TupleFrame> {
        let rt = invert_frame(self.frame);
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)], rt[(0,3)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)], rt[(1,3)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)], rt[(2,3)]),
            (0.0, 0.0, 0.0, 1.0)))
    }

    /// Add frames, Return a tuple that will be used to
    /// Create a Frame in Python
    pub fn _add(&self, other: Self) -> PyResult<TupleFrame> {
        let rt = self.frame + other.frame;
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)], rt[(0,3)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)], rt[(1,3)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)], rt[(2,3)]),
            (0.0, 0.0, 0.0, 1.0)))
    }

    /// subtract frames, Return a tuple that will be used to
    /// Create a Frame in Python
    pub fn _sub(&self, other: Self) -> PyResult<TupleFrame> {
        let rt = self.frame - other.frame;
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)], rt[(0,3)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)], rt[(1,3)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)], rt[(2,3)]),
            (0.0, 0.0, 0.0, 1.0)))
    }

    /// matrix product, Return a tuple that will be used to
    /// Create a Frame in Python
    pub fn _mul(&self, other: Self) -> PyResult<TupleFrame> {
        let rt = self.frame * other.frame;
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)], rt[(0,3)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)], rt[(1,3)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)], rt[(2,3)]),
            (0.0, 0.0, 0.0, 1.0)))
    }

    /// == overload
    pub fn __eq__(&self, other: Self) -> PyResult<bool> {
        Ok(self.frame == other.frame)
    }

    /// != overload
    pub fn __ne__(&self, other: Self) -> PyResult<bool> {
        Ok(self.frame != other.frame)
    }

    /// calculate the norm of the translation of the frame
    pub fn __abs__(&self) -> PyResult<f64> {
        let vector = self.frame.get_translation();
        Ok((vector[0].powi(2) + vector[1].powi(2) +
            vector[2].powi(2)).sqrt())
    }

    /// Get item operator
    pub fn __getitem__(&self, idx: (usize, usize)) -> PyResult<f64> {
        if idx.1 > 3 || idx.0 > 3 {
            Err(exceptions::IndexError::py_err("Index out of bounds"))
        } else {
            Ok(self.frame[idx])
        }
    }

    /// Set item operator
    pub fn __setitem__(&mut self, idx: (usize, usize), value:f64) -> PyResult<()> {
        if idx.0 > 3 || idx.1 > 3 {
            Err(exceptions::IndexError::py_err("Index out of bounds"))
        } else {
            self.frame[idx] = value;
            Ok(())
        }
    }

    /// Returns the identity 4x4 matrix, Return a tuple that will be used to
    /// Create a Frame in Python
    #[staticmethod]
    fn _identity() -> PyResult<TupleFrame> {
        Ok(((1.0, 0.0, 0.0, 0.0),
            (0.0, 1.0, 0.0, 0.0),
            (0.0, 0.0, 1.0, 0.0),
            (0.0, 0.0, 0.0, 1.0)))
    }

    /// Build a Frame from translation and Euler angles
    /// Return a tuple that will be used to
    /// Create a Frame in Python
    #[staticmethod]
    pub fn _from_translation_euler(
        x: f64,
        y: f64,
        z: f64,
        roll: f64,
        pitch: f64,
        yaw: f64,
    ) -> PyResult<TupleFrame> {
        let rt = KdlFrame::from_translation_euler(x, y, z, roll, pitch, yaw);
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)], rt[(0,3)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)], rt[(1,3)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)], rt[(2,3)]),
            (0.0, 0.0, 0.0, 1.0)))
    }

    /// Build a Frame from Euler angles and no translation
    /// Return a tuple that will be used to
    /// Create a Frame in Python
    #[staticmethod]
    pub fn _from_euler(roll: f64, pitch: f64, yaw: f64) -> PyResult<TupleFrame> {
        let rt = KdlFrame::from_euler(roll, pitch, yaw);
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)], rt[(0,3)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)], rt[(1,3)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)], rt[(2,3)]),
            (0.0, 0.0, 0.0, 1.0)))
    }

    /// Build a Frame from translation and no rotation
    /// Return a tuple that will be used to
    /// Create a Frame in Python
    #[staticmethod]
    pub fn _from_translation(x: f64, y: f64, z: f64) -> PyResult<TupleFrame> {
        let rt = KdlFrame::from_translation(x, y, z);
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)], rt[(0,3)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)], rt[(1,3)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)], rt[(2,3)]),
            (0.0, 0.0, 0.0, 1.0)))
    }

    /// Build a Frame from the rotation axis vector. The norm of the vector
    /// represents the angle of rotation. There is no translation.
    /// Return a tuple that will be used to
    /// Create a Frame in Python
    #[staticmethod]
    pub fn _from_axisangle(a:f64, b:f64, c:f64) -> PyResult<TupleFrame> {
        let rt = KdlFrame::from_axisangle(a, b, c);
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)], rt[(0,3)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)], rt[(1,3)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)], rt[(2,3)]),
            (0.0, 0.0, 0.0, 1.0)))
    }

    /// Build a Frame from translation and rotation axis vector. The norm of
    /// the vector represents the angle of rotation.
    /// Return a tuple that will be used to
    /// Create a Frame in Python
    #[staticmethod]
    pub fn _from_translation_axisangle(
        x: f64, y: f64, z: f64, a:f64, b:f64, c:f64) -> PyResult<TupleFrame> {
        let rt = KdlFrame::from_translation_axisangle(x, y, z, a, b, c);
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)], rt[(0,3)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)], rt[(1,3)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)], rt[(2,3)]),
            (0.0, 0.0, 0.0, 1.0)))
    }
}


#[pyproto]
impl PyObjectProtocol for Frame {
    fn __str__(&self) -> PyResult<String> {
        self._repr()
    }

    fn __repr__(&self) -> PyResult<String> {
        self._repr()
    }
}


#[pyproto]
impl PyIterProtocol for Frame {
    fn __iter__(slf: PyRefMut<Frame>) -> PyResult<Py<Frame>> {
        Ok(slf.into())
    }

    fn __next__(mut slf: PyRefMut<Frame>) -> PyResult<Option<FrameRow>> {
        Ok(slf._next())
    }
}
