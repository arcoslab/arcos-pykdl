// Copyright (c) 2020 Autonomous Robots and Cognitive Systems Laboratory
// Author: Daniel Garcia-Vaglio <degv364@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use pyo3::prelude::*;
use pyo3::exceptions;
use pyo3::types::PyString;

use arcos_kdl::kinematic_arm::KinematicArm as KdlArm;
use arcos_kdl::joint::JointType as KdlJointType;

use crate::frame::{Frame, TupleFrame};
use crate::twist::{Twist, TupleTwist};

fn from_string(name: &PyString) -> Result<KdlJointType, &str> {
    let result = name.to_string();
    if result.is_err() {
        Err("Unable to parse string")
    } else {
        match result.unwrap().into_owned().as_str() {
            "NoJoint" => Ok(KdlJointType::NoJoint),
            "TransX" => Ok(KdlJointType::TransX),
            "TransY" => Ok(KdlJointType::TransY),
            "TransZ" => Ok(KdlJointType::TransZ),
            "RotX" => Ok(KdlJointType::RotX),
            "RotY" => Ok(KdlJointType::RotY),
            "RotZ" => Ok(KdlJointType::RotZ),
            _ => Err("Invalid joint type name"),
        }
    }
}



/// Implementation of kinematic arms
///
/// These are the subset of kinematic chains that are used as robotic\
/// arms. With an improved API for ease of used compared to the one\
/// we have for kinematic chains. It is different from the chain because\
/// it has a shoulder segment (segment with no joint) at the beginning \
/// and then N mobile segments. Also has a base transform that represents\
/// the robotic body to which the arm is attached.\
///
/// This is the intended and recommended way of using ARCOS-KDL.
#[pyclass(subclass, name=_KinematicArm)]
pub struct KinematicArm {
    /// ARCOS-KDL kinematic arm
    pub arm: KdlArm,
}

#[pymethods]
impl KinematicArm {
    /// Create a new empty kinematic arm
    #[new]
    pub fn new() -> Self {
        Self {
            arm: KdlArm::default(),
        }
    }

    /// Set the base pose transformation.\
    /// `transform`: A Frame holding the base transform to set
    pub fn set_base_transform(&mut self, transform: &Frame) {
        self.arm.set_base_transform(transform.frame);
    }

    /// Set the base velocity.\
    /// `twist`: the twist of the base to set
    pub fn set_base_twist(&mut self, twist: &Twist) {
        self.arm.set_base_twist(twist.twist);
    }

    /// Add a shoulder to the kinematic arm. This will remove the current\
    /// chain and add a new one with this shoulder.\
    /// `shoulder_tf`: Frame holding the shoulder transform
    pub fn set_shoulder_transform(&mut self, shoulder_tf: &Frame) {
        self.arm.set_shoulder_transform(shoulder_tf.frame);
    }

    /// Add a new joint by Type and transform. See 'add_segment' for more info\
    /// `joint_type`: type name of joint\
    /// `transform`: transform of the segment\
    /// `limits`: mechanical limits of the joint (radians)\
    /// `speed_limits`: speed limit of the joint (radians/s)\
    pub fn add_segment_by_type_name(&mut self,
                                    type_name: &PyString,
                                    transform: &Frame,
                                    limits: (f64, f64),
                                    speed_limit: f64) -> PyResult<()> {
        let joint_type_result = from_string(type_name);
        if joint_type_result.is_err() {
            Err(exceptions::ValueError::py_err("Joint Type name invalid"))
        } else {
            Ok(self.arm.add_segment_by_type(
                joint_type_result.unwrap(), transform.frame, limits, speed_limit))
        }
    }

    /// Set how mush to avoid singularities for inverse kinematics.\
    /// It is a scalar from 0 to 1 where\
    /// 0 means low avoidance and high accuracy and 1 means high avoidance\
    /// and low accuracy.\
    /// `lambda`: singularity avoidance\
    pub fn set_singularity_avoidance_ratio(&mut self, lambda: f64) {
        self.arm.set_singularity_avoidance_ratio(lambda);
    }

    /// Get the current joint speeds (radians/s)\
    pub fn get_joint_speeds(&mut self) -> PyResult<Vec<f64>> {
        Ok(self.arm.get_joint_speeds())
    }

    /// Get the current joint state (radians)\
    pub fn get_joint_states(&mut self) -> PyResult<Vec<f64>> {
        Ok(self.arm.get_joint_states())
    }

    /// Set the speeds of each joint.\
    /// `joint_speeds`: vector with the speed to set (radians/s). Must be the same\
    /// length as movable joints in the arm\
    pub fn set_joint_speeds(&mut self, joint_speeds: Vec<f64>) {
        self.arm.set_joint_speeds(joint_speeds);
    }

    /// Set the state of each joint. If the joint approaches a limit, it will
    /// also handle the weights for the inverse kinematic solver.\
    /// `joint_states`: vector with the state to set (radians). Must be the same
    /// length as movable joints in the arm\
    /// `weight_when_close`: What weight to apply when a joint approaches a limit\
    pub fn set_joint_states(&mut self, joint_states: Vec<f64>, weight_when_close: f64) {
        self.arm.set_joint_states(joint_states, weight_when_close);
    }

    /// Get the end effector pose. Takes into consideration the pose of
    /// the base of the arm. Return a tuple that will be used to
    /// Create a Frame in Python
    pub fn _get_global_pose(&mut self) -> PyResult<TupleFrame> {
        let rt = self.arm.get_global_pose();
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)], rt[(0,3)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)], rt[(1,3)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)], rt[(2,3)]),
            (0.0, 0.0, 0.0, 1.0)))
    }

    /// Get the end effector twist. Takes into consideration the pose and
    /// twist of the base of the arm.
    /// Return a tuple that will be used to
    /// Create a Frame in Python
    pub fn _get_glonal_twist(&mut self) -> PyResult<TupleTwist> {
        let rt = self.arm.get_global_twist();
        Ok((rt[0], rt[1], rt[2], rt[3], rt[4], rt[5]))
    }

    /// Get the end effector local pose, from the shoulder to the end
    /// effector. Return a tuple that will be used to
    /// Create a Frame in Python
    pub fn _get_pose(&mut self) -> PyResult<TupleFrame> {
        let rt = self.arm.get_pose();
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)], rt[(0,3)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)], rt[(1,3)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)], rt[(2,3)]),
            (0.0, 0.0, 0.0, 1.0)))
    }

    /// Get the end effector local twist, from the shoulder to the
    /// end effector. Return a tuple that will be used to
    /// Create a Frame in Python
    pub fn _get_twist(&mut self) -> PyResult<TupleTwist> {
        let rt = self.arm.get_twist();
        Ok((rt[0], rt[1], rt[2], rt[3], rt[4], rt[5]))
    }

    /// Move the base.\
    pub fn move_base(&mut self, base_twist: Twist, dt: f64) {
        self.arm.move_base(base_twist.twist, dt);
    }

    /// Move the end effector a differential\
    /// `twist`: Required or commanded Twist\
    /// `dt`: time differential\
    pub fn cartesian_diff_move(&mut self, twist: Twist, dt:f64) {
        self.arm.cartesian_diff_move(twist.twist, dt);
    }

    /// Move to the goal pose in a straight line from the current pose
    /// to the goal.\
    ///
    /// `goal`: the goal pose\
    /// `speed`: the speed at which the end effector should move\
    /// `rot_speed`: the speed at which the end effector should rotate\
    /// `dt`: time differential\
    /// `timeout`: time after which the goal is unreachable\
    /// `translation_tolerance`: allowed translational error\
    /// `rotation_tolerance`: allowed rotational error\
    /// returns final state if reachable\
    pub fn cartesian_move(&mut self, goal: Frame, speed: f64,
                          rot_speed: f64, dt:f64, timeout: f64,
                          translation_tolerance: f64,
                          rotation_tolerance: f64) -> PyResult<()> {
        let move_result = self.arm.cartesian_move(goal.frame, speed,
                                                  rot_speed, dt, timeout,
                                                  translation_tolerance,
                                                  rotation_tolerance);
        if move_result.is_err() {
            Err(exceptions::ValueError::py_err("Goal unreachable"))
        } else {
            Ok(())
        }
    }
}
