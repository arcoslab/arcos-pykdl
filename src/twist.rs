// Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
// Author: Daniel Garcia-Vaglio <degv364@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use pyo3::prelude::*;
use pyo3::exceptions;
use pyo3::PyObjectProtocol;
use pyo3::PyIterProtocol;

use arcos_kdl::geometry::Twist as KdlTwist;

use crate::vector::TupleVector;

/// Tuple with same size as Twist
pub type TupleTwist = (f64, f64, f64, f64, f64, f64);

/// 6D velocity vector
#[pyclass(subclass, name=_Twist)]
#[derive(Clone, Copy)]
pub struct Twist {
    /// ARCOS-KDL Twist
    pub twist: KdlTwist,
    /// Used for iterations
    pub idx: usize,
}

#[pymethods]
impl Twist {
    /// Create a new Twist
    #[new]
    pub fn new(in_array: Vec<f64>) -> PyResult<Self> {
        if in_array.len() != 6 {
            Err(exceptions::ValueError::py_err("Wrong twist size"))
        } else {
            let mut tmp_twist = KdlTwist::zeros();
            for i in 0..6 {
                tmp_twist[i] = in_array[i];
            }
            Ok(Self { twist: tmp_twist, idx: 0 })
        }
    }

    /// Get the 'formal; string of this Twist
    fn _repr(&self) -> PyResult<String> {
        Ok(format!(
            "[{}, {}, {}, {}, {}, {}]",
            self.twist[0].to_string(),
            self.twist[1].to_string(),
            self.twist[2].to_string(),
            self.twist[3].to_string(),
            self.twist[4].to_string(),
            self.twist[5].to_string()))
    }

    fn _next(&mut self) -> Option<f64> {
        if self.idx < 6 {
            self.idx += 1;
            Some(self.twist[self.idx-1])
        } else {
            self.idx = 0;
            None
        }
    }


    /// Get the vector representing the translation of the Twist
    pub fn _get_translation(&self) -> PyResult<TupleVector> {
        Ok((self.twist[0], self.twist[1], self.twist[2]))
    }

    /// Get the Rotation representing the rotation of the Twist
    pub fn _get_rotation(&self) -> PyResult<TupleVector> {
        Ok((self.twist[3], self.twist[4], self.twist[5]))
    }

    /// Add twists,  Return a tuple that will be used to
    /// Create a Vector in Python
    pub fn _add(&self, other: Self) -> PyResult<TupleTwist> {
        let rt = self.twist + other.twist;
        Ok((rt[0], rt[1], rt[2], rt[3], rt[4], rt[5]))
    }

    /// subtract twists,  Return a tuple that will be used to
    /// Create a Vector in Python
    pub fn _sub(&self, other: Self) -> PyResult<TupleTwist> {
        let rt = self.twist - other.twist;
        Ok((rt[0], rt[1], rt[2], rt[3], rt[4], rt[5]))
    }

    /// dot product
    pub fn __mul__(&self, other: Self) -> PyResult<f64> {
        Ok(self.twist[0] * other.twist[0] +
           self.twist[1] * other.twist[1] +
           self.twist[2] * other.twist[2] +
           self.twist[3] * other.twist[3] +
           self.twist[4] * other.twist[4] +
           self.twist[5] * other.twist[5])
    }

    /// == overload
    pub fn __eq__(&self, other: Self) -> PyResult<bool> {
        Ok(self.twist == other.twist)
    }

    /// != overload
    pub fn __ne__(&self, other: Self) -> PyResult<bool> {
        Ok(self.twist != other.twist)
    }

    /// calculate the norm of a Twist
    pub fn __abs__(&self) -> PyResult<f64> {
        Ok((self.twist[0].powi(2) + self.twist[1].powi(2) +
            self.twist[2].powi(2) + self.twist[3].powi(2) +
            self.twist[4].powi(2) + self.twist[5].powi(2)).sqrt())
    }

    /// Get item operator
    pub fn __getitem__(&self, idx: usize) -> PyResult<f64> {
        if idx > 5 {
            Err(exceptions::IndexError::py_err("Index out of bounds"))
        } else {
            Ok(self.twist[idx])
        }
    }

    /// Set item operator
    pub fn __setitem__(&mut self, idx: usize, value:f64) -> PyResult<()> {
        if idx > 5 {
            Err(exceptions::IndexError::py_err("Index out of bounds"))
        } else {
            self.twist[idx] = value;
            Ok(())
        }
    }

    /// Get the length
    pub fn __len__(&self) -> PyResult<i32> {
        Ok(6)
    }
}


#[pyproto]
impl PyObjectProtocol for Twist {
    fn __str__(&self) -> PyResult<String> {
        self._repr()
    }

    fn __repr__(&self) -> PyResult<String> {
        self._repr()
    }
}


#[pyproto]
impl PyIterProtocol for Twist {
    fn __iter__(slf: PyRefMut<Twist>) -> PyResult<Py<Twist>> {
        Ok(slf.into())
    }

    fn __next__(mut slf: PyRefMut<Twist>) -> PyResult<Option<f64>> {
        Ok(slf._next())
    }
}
