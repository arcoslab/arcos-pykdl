// Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
// Author: Daniel Garcia-Vaglio <degv364@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


use pyo3::prelude::*;
use pyo3::exceptions;
use pyo3::PyObjectProtocol;
use pyo3::PyIterProtocol;

use arcos_kdl::geometry::Vector as KdlVector;

/// Tuple with same size as vector
pub type TupleVector = (f64, f64, f64);

/// 3D Vector
#[pyclass(subclass, name=_Vector)]
#[derive(Clone)]
pub struct Vector {
    /// ARCOS-KDL 3D vector
    pub vector: KdlVector,
    /// Used for iterations
    pub idx: usize,
}

#[pymethods]
impl Vector {
    /// Create a new Vector
    #[new]
    pub fn new(in_array: Vec<f64>) -> PyResult<Self> {
        if in_array.len() != 3 {
            Err(exceptions::ValueError::py_err("Wrong number of rows"))
        } else {
            Ok(Self {vector: KdlVector::new(
                in_array[0], in_array[1], in_array[2]),
                     idx: 0})
        }
    }

    /// Get the 'formal; string of this vector
    fn _repr(&self) -> PyResult<String> {
        Ok(format!(
            "[{}, {}, {}]",
            self.vector[0].to_string(),
            self.vector[1].to_string(),
            self.vector[2].to_string()))
    }

    fn _next(&mut self) -> Option<f64> {
        if self.idx < 3 {
            self.idx += 1;
            Some(self.vector[self.idx-1])
        } else {
            self.idx = 0;
            None
        }
    }


    /// Add Two vectors, Return a tuple that will be used to
    /// Create a Vector in Python
    pub fn _add(&self, other: Self) -> PyResult<TupleVector> {
        let result = self.vector + other.vector;
        Ok((result[0], result[1], result[2]))
    }

    /// subtract two vectors, Return a tuple that will be used to
    /// Create a Vector in Python
    pub fn _sub(&self, other: Self) -> PyResult<TupleVector> {
        let result = self.vector - other.vector;
        Ok((result[0], result[1], result[2]))
    }

    /// dot product
    pub fn __mul__(&self, other: Self) -> PyResult<f64> {
        Ok(self.vector[0] * other.vector[0] +
           self.vector[1] * other.vector[1] +
           self.vector[2] * other.vector[2])
    }

    /// == overload
    pub fn __eq__(&self, other: Self) -> PyResult<bool> {
        Ok(self.vector == other.vector)
    }

    /// != overload
    pub fn __ne__(&self, other: Self) -> PyResult<bool> {
        Ok(self.vector != other.vector)
    }

    /// calculate the norm of a vector
    pub fn __abs__(&self) -> PyResult<f64> {
        Ok((self.vector[0].powi(2) + self.vector[1].powi(2) +
            self.vector[2].powi(3)).sqrt())
    }

    /// Get item operator
    pub fn __getitem__(&self, idx: usize) -> PyResult<f64> {
        if idx > 2 {
            Err(exceptions::IndexError::py_err("Index out of bounds"))
        } else {
            Ok(self.vector[idx])
        }
    }

    /// Set item operator
    pub fn __setitem__(&mut self, idx: usize, value:f64) -> PyResult<()> {
        if idx > 2 {
            Err(exceptions::IndexError::py_err("Index out of bounds"))
        } else {
            self.vector[idx] = value;
            Ok(())
        }
    }

    /// Get the length
    pub fn __len__(&self) -> PyResult<i32> {
        Ok(3)
    }

}

#[pyproto]
impl PyObjectProtocol for Vector {
    fn __str__(&self) -> PyResult<String> {
        self._repr()
    }

    fn __repr__(&self) -> PyResult<String> {
        self._repr()
    }
}


#[pyproto]
impl PyIterProtocol for Vector {
    fn __iter__(slf: PyRefMut<Vector>) -> PyResult<Py<Vector>> {
        Ok(slf.into())
    }

    fn __next__(mut slf: PyRefMut<Vector>) -> PyResult<Option<f64>> {
        Ok(slf._next())
    }
}
