// Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
// Author: Daniel Garcia-Vaglio <degv364@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#![deny(missing_docs)]

/*!
# ARCOS-KDL

## ARCOS-Lab Kinematics and Dynamics Library

These are the python bindings of the library
(originally in Rust).

[arcos-kdl](https://gitlab.com/arcoslab/arcos-kdl)

*/

use pyo3::prelude::*;
use pyo3::wrap_pymodule;

/// 3D vector
pub mod vector;
use crate::vector::Vector;

/// 3D rotation
pub mod rotation;
use crate::rotation::Rotation;

/// 6D vector
pub mod twist;
use crate::twist::Twist;

/// 4D Homogeneous matrix
pub mod frame;
use crate::frame::Frame;

/// Basic geometric constructs
pub mod geometry;
use crate::geometry::PyInit__geometry;

/// Implementation of a kinematic robotic arm
pub mod kinematic_arm;
use crate::kinematic_arm::KinematicArm;

/// The arcos-kdl python module
#[pymodule]
pub fn arcos_pykdl(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_class::<Vector>()?;
    m.add_class::<Twist>()?;
    m.add_class::<Rotation>()?;
    m.add_class::<Frame>()?;
    m.add_wrapped(wrap_pymodule!(_geometry))?;
    m.add_class::<KinematicArm>()?;
    Ok(())
}
