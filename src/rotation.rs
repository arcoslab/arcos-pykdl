// Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
// Author: Daniel Garcia-Vaglio <degv364@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use pyo3::prelude::*;
use pyo3::exceptions;
use pyo3::PyObjectProtocol;
use pyo3::PyIterProtocol;

use arcos_kdl::geometry::Rotation as KdlRotation;

use crate::vector::TupleVector;

/// Tuple with same size as rotation
pub type TupleRotation = ((f64, f64, f64), (f64, f64, f64), (f64, f64, f64));

/// Determine if the size of a matrix fits the description
fn is_matrix_size_correct(matrix: Vec<Vec<f64>>) -> bool{
    if matrix.len() != 3 {
        false
    } else {
        matrix[0].len() == 3 && matrix[1].len() == 3 && matrix[2].len() == 3
    }
}


/// 3D rotation Matrix
#[pyclass(subclass, name=_Rotation)]
#[derive(Clone)]
pub struct Rotation {
    /// ARCOS-KDL 3D rotation matrix
    pub rotation: KdlRotation,
    /// Used for iterating
    pub idx: usize,
}

#[pymethods]
impl Rotation {
    /// Create a new Rotation
    #[new]
    pub fn new(in_array: Vec<Vec<f64>>) -> PyResult<Self> {
        if !is_matrix_size_correct(in_array.clone()) {
            Err(exceptions::ValueError::py_err("Wrong matrix size"))
        } else {
            let mut tmp_rotation = KdlRotation::zeros();
            for i in 0..3 {
                for j in 0..3 {
                    tmp_rotation[(i, j)] = in_array[i][j];
                }
            }
            Ok(Self {rotation: tmp_rotation, idx: 0})
        }
    }

    /// Get the 'formal; string of this Rotation
    fn _repr(&self) -> PyResult<String> {
        Ok(format!(
            "[[{}, {}, {}],\n [{}, {}, {}],\n [{}, {}, {}]]",
            self.rotation[(0,0)].to_string(),
            self.rotation[(0,1)].to_string(),
            self.rotation[(0,2)].to_string(),
            self.rotation[(1,0)].to_string(),
            self.rotation[(1,1)].to_string(),
            self.rotation[(1,2)].to_string(),
            self.rotation[(2,0)].to_string(),
            self.rotation[(2,1)].to_string(),
            self.rotation[(2,2)].to_string()))
    }

    fn _next(&mut self) -> Option<TupleVector> {
        if self.idx < 3 {
            self.idx += 1;
            Some((self.rotation[(self.idx-1, 0)],
                  self.rotation[(self.idx-1, 1)],
                  self.rotation[(self.idx-1, 2)]))
        } else {
            self.idx = 0;
            None
        }
    }

    /// Add overload, Return a tuple that will be used to
    /// Create a Rotation in Python
    pub fn _add(&self, other: Self) -> PyResult<TupleRotation> {
        let rt = self.rotation + other.rotation;
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)])))
    }

    /// sub overload, Return a tuple that will be used to
    /// Create a Rotation in Python
    pub fn _sub(&self, other: Self) -> PyResult<TupleRotation> {
        let rt = self.rotation - other.rotation;
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)])))
    }

    /// dot product, Return a tuple that will be used to
    /// Create a Rotation in Python
    pub fn _mul(&self, other: Self) -> PyResult<TupleRotation> {
        let rt = self.rotation * other.rotation;
        Ok(((rt[(0,0)], rt[(0,1)], rt[(0,2)]),
            (rt[(1,0)], rt[(1,1)], rt[(1,2)]),
            (rt[(2,0)], rt[(2,1)], rt[(2,2)])))
    }

    /// == overload
    pub fn __eq__(&self, other: Self) -> PyResult<bool> {
        Ok(self.rotation == other.rotation)
    }

    /// != overload
    pub fn __ne__(&self, other: Self) -> PyResult<bool> {
        Ok(self.rotation != other.rotation)
    }

    /// calculate the determinant of a Rotation (always 1)
    pub fn __abs__(&self) -> PyResult<f64> {
        Ok(1.0)
    }

    /// Get item operator
    pub fn __getitem__(&self, idx: (usize, usize)) -> PyResult<f64> {
        if idx.1 > 2 || idx.0 > 2 {
            Err(exceptions::IndexError::py_err("Index out of bounds"))
        } else {
            Ok(self.rotation[idx])
        }
    }

    /// Set item operator
    pub fn __setitem__(&mut self, idx: (usize, usize), value:f64) -> PyResult<()> {
        if idx.0 > 2 || idx.1 > 2{
            Err(exceptions::IndexError::py_err("Index out of bounds"))
        } else {
            self.rotation[idx] = value;
            Ok(())
        }
    }
}

#[pyproto]
impl PyObjectProtocol for Rotation {
    fn __str__(&self) -> PyResult<String> {
        self._repr()
    }

    fn __repr__(&self) -> PyResult<String> {
        self._repr()
    }
}


#[pyproto]
impl PyIterProtocol for Rotation {
    fn __iter__(slf: PyRefMut<Rotation>) -> PyResult<Py<Rotation>> {
        Ok(slf.into())
    }

    fn __next__(mut slf: PyRefMut<Rotation>) -> PyResult<Option<TupleVector>> {
        Ok(slf._next())
    }
}
