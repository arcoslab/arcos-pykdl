# Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from .vector import Vector
from .arcos_pykdl import _Rotation


class Rotation(_Rotation):
    def __add__(self, other):
        return Rotation(self._add(other))

    def __sub__(self, other):
        return Rotation(self._sub(other))

    def __mul__(self, other):
        return Rotation(self._mul(other))

    def __next__(self):
        return Vector(super().__next__())
