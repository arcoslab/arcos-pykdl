# Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from .vector import Vector
from .rotation import Rotation
from .arcos_pykdl import _Frame


class Frame(_Frame):
    def get_translation(self):
        """
        Get the vector representing the translation of the Frame
        """
        return Vector(self._get_translation())

    def get_rotation(self):
        """
        Get the Rotation representing the rotation of the Frame
        """
        return Rotation(self._get_rotation())

    def inverse(self):
        """ Compute the inverse matrix """
        return Frame(self._inverse())

    def __add__(self, other):
        return Frame(self._add(other))

    def __sub__(self, other):
        return Frame(self._sub(other))

    def __mul__(self, other):
        return Frame(self._mul(other))

    @staticmethod
    def identity():
        """ Returns the 4x4 identity matrix """
        return Frame(_Frame._identity())

    @staticmethod
    def from_translation_euler(x, y, z, roll, pitch, yaw):
        """ Build a Frame from translation and Euler angles """
        return Frame(_Frame._from_translation_euler(
            x, y, z, roll, pitch, yaw))

    @staticmethod
    def from_euler(roll, pitch, yaw):
        """ Build a Frame from Euler angles and no translation """
        return Frame(_Frame._from_euler(roll, pitch, yaw))

    @staticmethod
    def from_translation(x, y, z):
        """ Build a Frame from translation and no rotation """
        return Frame(_Frame._from_translation(x, y, z))

    @staticmethod
    def from_axisangle(a, b, c):
        """
        Build a Frame from the rotation axis vector. The norm of the vector
        represents the angle of rotation. There is no translation.
        """
        return Frame(_Frame._from_axisangle(a, b, c))

    @staticmethod
    def from_translation_axisangle(x, y, z, a, b, c):
        """
        Build a Frame from translation and rotation axis vector. The norm of
        the vector represents the angle of rotation.
        """
        return Frame(_Frame._from_translation_axisangle(
            x, y, z, a, b, c))
