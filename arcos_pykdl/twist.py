# Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from .arcos_pykdl import _Twist
from .vector import Vector


class Twist(_Twist):
    def __add__(self, other):
        return Twist(self._add(other))

    def __sub__(self, other):
        return Twist(self._sub(other))

    def get_translation(self):
        """
        Get the translational part of the twist
        """
        return Vector(self._get_translation())

    def get_rotation(self):
        """
        Get the rotational part of the twist
        """
        return Vector(self._get_rotation())
