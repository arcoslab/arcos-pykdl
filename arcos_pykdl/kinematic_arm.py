# Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from .arcos_pykdl import _KinematicArm
from .twist import Twist
from .frame import Frame


class KinematicArm(_KinematicArm):
    def get_global_pose(self):
        """
        Get the end effector pose. Takes into consideration the pose of
        the base of the arm.
        """
        return Frame(self._get_global_pose())

    def get_global_twist(self):
        """
        Get the end effector twist. Takes into consideration the pose and
        twist of the base of the arm.
        """
        return Twist(self._get_global_twist())

    def get_pose(self):
        """
        Get the end effector local pose, from the shoulder to the end
        effector.
        """
        return Frame(self._get_pose())

    def get_twist(self):
        """
        Get the end effector local twist, from the shoulder to the
        end effector.
        """
        return Twist(self._get_twist())
